# ExpressToll

After cloning the repo you can run a few lines of R code to visualize what the toll expenditures look like.

### R Code 

#### Tolls over time
<details>
<summary><b>Tolls over time code </b></summary>

```r
library(lubridate)
library(ggplot2)
library(dplyr)


mydata <- read.csv(file = "tolls.csv")
names(mydata) <- gsub(pattern = "\\.",replacement = "",x = names(mydata))
mydata$TransactionDateTime <- strptime(mydata$TransactionDateTime,format = "%m-%d-%Y %H:%M:%S")

mydata$TransactionDateTime <- as.POSIXct(mydata$TransactionDateTime )
mydata$Date <- as.Date(mydata$TransactionDateTime )
mydata$Amount <-as.numeric( gsub(pattern = "\\$",replacement = "",
                                 x = mydata$Amount) )
mydata$Week <- floor_date(mydata$Date, unit = "week")


bigm <- function(x) paste0("$",formatC(x = x,digits = 0,big.mark = ",",format = "d"))
TotalTolls <- sum(mydata$Amount)

png("TollsByWeek.png")
mydata %>%
  group_by(Week) %>%
  summarise(Total = sum(Amount)) %>%
  data.frame() %>% 
  ggplot( aes(y=Total,x=Week)) + 
  # geom_line() +
  geom_smooth(color="cyan4") +
  geom_point(color="black") + 
  ggtitle(label = paste0("Total Tolls = ",bigm(TotalTolls) ) ) 
dev.off()
```
</details>

![](TollsByWeek.png)


```r

p <- mydata %>%
  group_by(Year_Month) %>%
  summarise(Total = sum(Amount)) %>%
  data.frame() %>% 
  ggplot( aes(y=Total,x=Year_Month)) + 
  # geom_line() +
  geom_smooth(color="cyan4") +
  geom_point(color="black") + 
  ggtitle(label = paste0("Total Tolls = ",bigm(TotalTolls) ) ) 
# dev.off()
library(plotly)
ggplotly(p)

# Avg Per Month
mydata %>%
  group_by(Year_Month) %>%
  summarise(Total = sum(Amount)) %>%
  summarise( avg = mean(Total) ,
             std = sd(Total)) %>%
  data.frame() 
  ```
  
#### Tolls By Week 
<details>
<summary><b>Tolls by Week Code </b></summary>

```r
# Add day of week 
mydata$day <- weekdays(mydata$Date)

library(sqldf)


plotData <- mydata %>% filter(day!="Saturday") 
workWeek <- data.frame(day=c("Monday","Tuesday","Wednesday","Thursday","Friday"),
                       num = c(1,2,3,4,5) )
plotData <- sqldf("
             Select 
                  sum(p.Amount) as Amount,
                  Count(distinct Date) as daysDriving,
                  p.day
             From plotData p
             inner join 
             workWeek
             on p.day= workWeek.day
             Group By 
              p.day
            Order By Amount desc")
plotData$cumsum <- cumsum(plotData$Amount)
plotData$avg <- plotData$Amount /plotData$daysDriving  
# plotData$sd <- plotData$Amount /plotData$daysDriving  

png("pareto.png")
bar <- barplot(plotData$Amount, 
        las=1,
        col="cyan4",
        width = 1, space = 0.2, border = NA, axes = F,
        ylim = c(0, 1.05 * max(plotData$cumsum, na.rm = T)), 
        # ylab = "Cummulative Dollars" , 
        cex.names = 0.7, 
          names.arg = plotData$day,
        main = "Tolls Per Day")
lines(bar,
      plotData$cumsum, 
      type = "b", 
      cex = 0.7, 
      pch = 19, 
      col="cyan4",
      lwd=2
      ) 
axis(side = 2, 
     at = axTicks(2), 
     las = 1, 
     # col.axis = "grey62", 
     # col = "grey62", 
     tick = T, 
     cex.axis = 0.8,
     labels = bigm(axTicks(2) ) 
     )
dev.off()
```

</details>




### Tolls By Day of Week
![](pareto.png)
